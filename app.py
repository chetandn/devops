from flask import Flask, jsonify
app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello World to you!"

@app.route('/help')
def help_page():
    return "all help topic here"

@app.route('/bye')
def bye():
    #prepare as response
    c = 2 * 534
    s = str(c)
    age = 14 * 2
    # c = 1/0

    retJson = {
        'field1':'abc',
        'field2':'def'
    }

    retJson2 = {
        "Name" : "Chetan Naik",
        "Age" : age, # dynamically getting the data 
        "phone" : [
            {
                "phone" : "iphone",
                "number" : 777
            },
            {
                "phone" : "satPhone",
                "number" : 000
            }
        ]
    }

    return jsonify(retJson2)

if __name__ == "__main__":
    app.run(debug=True)
